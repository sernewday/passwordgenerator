<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>ET OOP Form</title>
    <meta name="description" content="Webix Carousel">
    <meta name="author" content="Serhii Aksonov">
    <link href="images/favicon.png" rel="shortcut icon" />
    <link rel="stylesheet" href="https://cdn.webix.com/edge/webix.css" type="text/css" />
    <script src="https://cdn.webix.com/edge/webix.js" type="text/javascript"></script>
</head>

<body>
<script type="text/javascript">
function Carousel() {
    var id = Math.floor(Math.random() * Math.floor(1000)); 
    var data = [
      { css: "image", template:img, data:{src:"http://shopping.loc/uploads/products/su/suknya-5606-87510664849765_small12.jpg"} },
      { css: "image", template:img, data:{src:"http://shopping.loc/uploads/products/su/suknya-5606-60938189105441_small12.jpg"} },
      { css: "image", template:img, data:{src:"http://shopping.loc/uploads/products/su/suknya-5606-70313521964997_small12.jpg"} },
      { css: "image", template:img, data:{src:"http://shopping.loc/uploads/products/su/suknya-5606-87510664849765_small12.jpg"} },
      { css: "image", template:img, data:{src:"http://shopping.loc/uploads/products/su/suknya-5606-60938189105441_small12.jpg"} },
      { css: "image", template:img, data:{src:"http://shopping.loc/uploads/products/su/suknya-5606-70313521964997_small12.jpg"} },
    ];
     
            var carshow = webix.ui({
            view:"window",
            id: id,
            body:{
               view:"carousel",
                id: "carousel",
                width:415, height:602, 
                cols:data
              },
              head:{
                view:"toolbar", type:"MainBar", elements:[
                {view:"label", label: " Платье Cat Orange 5606 42 Мультиколор", align:'center'},
                {view:"icon", icon:"wxi-close", tooltip:"Close window", click: function(){
                  carshow.close();
              }}
                ]
            },
            position:"center",
            }); 
            carshow.show();

function img(obj){
  return '<img src="'+obj.src+'"  width="100%" height="100%" ondragstart="return false"/>'
}    

}

Carousel();

</script>
</body>

</html>