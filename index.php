<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>ET Password Generator</title>
  <meta name="description" content="ET Password Generator">
  <meta name="author" content="Serhii Aksonov">
  <link href="images/favicon.png" rel="shortcut icon" />
  <link rel="stylesheet" href="https://cdn.webix.com/edge/webix.css" type="text/css" />
  <script src="https://cdn.webix.com/edge/webix.js" type="text/javascript"></script>
</head>

<body>
  <script type="text/javascript">
    const low_letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
    const hi_letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
    const numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
    const special_simbols = [',', '.', ':', '~', '%', '#', '*', '&', '^', '@'];

    const toolbar = {
      view: "toolbar",
      css: "webix_dark",
      cols: [{
        id: "label",
        view: "label",
        label: "Password Generator"
      }]
    };

    webix.ui({
      width: 500,
      height: 150,
      type: "space",
      css: {
        margin: "auto",
        position: "center",

      },
      rows: [
        toolbar,
        {
          cells: [{
            id: "body",
            height: 70,
            cols: [{
                view: "button",
                id: "generate_button",
                value: "Generate",
                click: function(id, event) {
                  generate();
                }
              },
              {
                id: "Outnput",
                view: "text",
                label: "Result",
                labelPosition: "top",
                value: "",
              },
            ]
          }, ]
        }
      ]
    });

    function generate() {
      var password = '';
      for (var i = 1; i < 9; i++) {
        let get_switch_number = getSwitchNumber(0, 3);
        let get_rendom_symbol = getRendomSymbol(get_switch_number);
        password += get_rendom_symbol
      }
      $$("Outnput").setValue(password);
    }

    function getSwitchNumber(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    function getRendomSymbol(rand_switch) {
      let response = 'y';
      switch (rand_switch) {
        case 0:
          response = special_simbols[getSwitchNumber(0, 9)];
          break;
        case 1:
          response = numbers[getSwitchNumber(0, 9)];
          break;
        case 2:
          response = hi_letters[getSwitchNumber(0, 25)];
          break;
        case 3:
          response = low_letters[getSwitchNumber(0, 25)];
          break;
      }

      return response;
    }
  </script>
</body>

</html>