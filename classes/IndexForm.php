<?php
class IndexForm extends AbstractForm
{
    function __construct() {
      }
      //Set form fields with type, but sorry, its hardcode, becouse its fast test
      public function createForm(){
        $login = '';
        $password = '';
        $phone = '';
        $email = '';
        $paycard = '';
        $expdate = '';
        $cvvcode = '';

        $form = array('login' => $login, 'password' => $password, 'phone' => $phone, 'email' => $email, 
          'paycard' => $paycard, 'expdate' => $expdate,'cvvcode' => $cvvcode);

        return $form;
    }
    
    //get data from client and fill form
    public function getFormData(){
        $form = $this->createForm();
        $formdata = array();

        foreach ($form as $key => $value):
            $formdata[$key] = array(0 => $value[$key], 1=>$_REQUEST[$key]);
        endforeach;

        return $formdata;
    }
}